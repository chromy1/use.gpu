declare module "@use-gpu/wgsl/instance/fragment/sdf-rectangle.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getSDFRectangleFragment: ParsedBundle;
  export default __module;
}
