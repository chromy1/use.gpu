declare module "@use-gpu/wgsl/instance/index/anchor.wgsl" {
  type ParsedBundle = import('@use-gpu/shader').ParsedBundle;
  const __module: ParsedBundle;
  export const getAnchorIndex: ParsedBundle;
  export default __module;
}
